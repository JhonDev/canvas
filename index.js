var c = document.getElementById("miCanvas");
var ctx = c.getContext("2d");
ctx.fillStyle = "#FFED33";
ctx.beginPath();
ctx.arc(300, 100, 40, 0, 2*Math.PI);
ctx.fill();

var d = document.getElementById("miCanvas");
var xd = d.getContext("2d")
xd.fillSyles = "000000";
xd.beginPath();

// start base
xd.moveTo(60, 380);
xd.lineTo(490, 380);
xd.closePath();
xd.stroke();
// end base

// start pino
xd.beginPath();
xd.moveTo(100, 380)
xd.lineTo(100, 250)
xd.closePath();
xd.stroke();

xd.beginPath();
xd.moveTo(440, 380)
xd.lineTo(440, 250)
xd.closePath();
xd.stroke();
// end pino

// start seed1
xd.beginPath();
xd.moveTo(100, 250)
xd.lineTo(75, 280)
xd.closePath();
xd.stroke();
// end seed

// start seed
xd.beginPath();
xd.moveTo(100, 280)
xd.lineTo(75, 310)
xd.closePath();
xd.stroke();
// end seed

// start seed
xd.beginPath();
xd.moveTo(100, 310)
xd.lineTo(75, 340)
xd.closePath();
xd.stroke();
// end seed

// start seed
xd.beginPath();
xd.moveTo(125, 280)
xd.lineTo(100, 250)
xd.closePath();
xd.stroke();
// end seed

// start seed
xd.beginPath();
xd.moveTo(125, 280)
xd.lineTo(100, 250)
xd.closePath();
xd.stroke();
// end seed

// start seed
xd.beginPath();
xd.moveTo(125, 310)
xd.lineTo(100, 280)
xd.closePath();
xd.stroke();
// end seed

// start seed
xd.beginPath();
xd.moveTo(125, 310)
xd.lineTo(100, 280)
xd.closePath();
xd.stroke();
// end seed

// start seed
xd.beginPath();
xd.moveTo(100, 310)
xd.lineTo(125, 340)
xd.closePath();
xd.stroke();
// end seed

// start seed1
xd.beginPath();
xd.moveTo(100, 310)
xd.lineTo(125, 340)
xd.closePath();
xd.stroke();
// end seed

xd.beginPath();
xd.moveTo(400, 380)
xd.lineTo(400, 250)
xd.closePath();
xd.stroke();

// start seed2
xd.beginPath();
xd.moveTo(400, 310)
xd.lineTo(375, 340)
xd.closePath();
xd.stroke();
// end seed

// start seed
xd.beginPath();
xd.moveTo(400, 310)
xd.lineTo(425, 340)
xd.closePath();
xd.stroke();
// end seed

// start seed
xd.beginPath();
xd.moveTo(425, 310)
xd.lineTo(400, 280)
xd.closePath();
xd.stroke();
// end seed

// start home
xd.beginPath();
xd.moveTo(150, 380)
xd.lineTo(150, 250)
xd.closePath();
xd.stroke();

// start seed
xd.beginPath();
xd.moveTo(425, 280)
xd.lineTo(400, 250)
xd.closePath();
xd.stroke();
// end seed

// start seed
xd.beginPath();
xd.moveTo(400, 280)
xd.lineTo(375, 310)
xd.closePath();
xd.stroke();
// end seed

// start seed1
xd.beginPath();
xd.moveTo(400, 250)
xd.lineTo(375, 280)
xd.closePath();
xd.stroke();
// end seed

// start seed1
xd.beginPath();
xd.moveTo(440, 250)
xd.lineTo(415, 280)
xd.closePath();
xd.stroke();
// end seed

// start seed1
xd.beginPath();
xd.moveTo(440, 280)
xd.lineTo(415, 310)
xd.closePath();
xd.stroke();
// end seed

// start seed1
xd.beginPath();
xd.moveTo(440, 280)
xd.lineTo(415, 310)
xd.closePath();
xd.stroke();
// end seed

// start seed1
xd.beginPath();
xd.moveTo(440, 280)
xd.lineTo(465, 310)
xd.closePath();
xd.stroke();
// end seed

// start seed1
xd.beginPath();
xd.moveTo(440, 310)
xd.lineTo(465, 340)
xd.closePath();
xd.stroke();
// end seed

// start seed1
xd.beginPath();
xd.moveTo(440, 310)
xd.lineTo(415, 340)
xd.closePath();
xd.stroke();
// end seed

// start seed1
xd.beginPath();
xd.moveTo(440, 250)
xd.lineTo(465, 280)
xd.closePath();
xd.stroke();
// end seed

xd.beginPath();
xd.moveTo(250, 200)
xd.lineTo(150, 250)
xd.closePath();
xd.stroke();

xd.beginPath();
xd.moveTo(250, 200);
xd.lineTo(350, 250);
xd.closePath();
xd.stroke();

xd.beginPath();
xd.moveTo(350, 380);
xd.lineTo(350, 250);
xd.closePath();
xd.stroke();
// start roof
xd.beginPath();
xd.moveTo(350, 250);
xd.lineTo(370, 250);
xd.closePath();
xd.stroke();

xd.beginPath();
xd.moveTo(150, 250);
xd.lineTo(130, 250);
xd.closePath();
xd.stroke();

xd.beginPath();
xd.moveTo(350, 250);
xd.lineTo(370, 250);
xd.closePath();
xd.stroke();

xd.beginPath();
xd.moveTo(250, 200);
xd.lineTo(350, 250);
xd.closePath();
xd.stroke();

xd.beginPath();
xd.moveTo(250, 185);
xd.lineTo(370, 250);
xd.closePath();
xd.stroke();

xd.beginPath();
xd.moveTo(250, 185);
xd.lineTo(130, 250);
xd.closePath();
xd.stroke();

//end roof
//start door

xd.beginPath();
xd.moveTo(225, 280);
xd.lineTo(225, 380);
xd.closePath();
xd.stroke();

xd.beginPath();
xd.moveTo(275, 280);
xd.lineTo(275, 380);
xd.closePath();
xd.stroke();

xd.beginPath();
xd.moveTo(275, 280);
xd.lineTo(225, 280);
xd.closePath();
xd.stroke();

// end door
